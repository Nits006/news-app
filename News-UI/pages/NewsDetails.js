import Head from 'next/head'
import styles from '../styles/Home.module.css'

import { useAmp } from 'next/amp' 
import axios from "axios";
export const config = { amp: 'hybrid' }
import Image from 'next/image'



function NewsDetails({ results }) {
  const isAmp = useAmp()
  return (
    <div className={styles.container}>
      <Head>
        <title>News Details</title>
        <link rel="icon" href="/favicon.ico" />
        <meta name="viewport" content="width=device-width,minimum-scale=1,initial-scale=1" />

      </Head>

      <main className={styles.main} style={{ paddingTop: "5px", textAlign: "center" }}>
        <h2 style={{}}>
          Welcome to <a href="javascript:void(0)">News Details</a>
        </h2>
        <div className={styles.grid} style={{ marginTop: "0px", }}>

          <p style={{ minWidth: "800px", }} className={styles.description}>
            <code className={styles.code}>  Category -
               {results.category_name} </code>
          </p>
          <a href="#" className={styles.card}>
            {isAmp ? (
              <amp-img
                width="200"
                height="200"
                src={results.image_url ? results.image_url : 'https://irishmediaman.files.wordpress.com/2012/03/news.jpg'}
                // src="https:///ui/images/office.png"
                alt="Image"
                layout="responsive"
              />
            ) : (
                <img width="300" height="300" src={results.image_url ? results.image_url : 'https://irishmediaman.files.wordpress.com/2012/03/news.jpg'} alt="a cool image" />
              )}
          </a>
          <hr style={{ minWidth: "800px", }}></hr>
          <div className={styles.grid}>
            <h4 style={{ margin: "2px", }}>Headline &rarr;</h4>
            <p style={{ minWidth: "800px", }}>{results.headline}</p>

            <h5 style={{ margin: "2px", }}>Summary</h5>
            <p style={{ minWidth: "800px", }}>{results.summary}</p>

            <a style={{ padding: "2px", minWidth: "800px", textAlign: "center" }} href="#" className={styles.card}>
              <h6>Tags &rarr;  {results.tags}</h6>
            </a>


          </div>


        </div>

      </main>

      <footer className={styles.footer} style={{ textAlign: "center" }}>

        Powered by {'News'}

      </footer>
    </div>
  )
}

export async function getServerSideProps({ query }) {
  try {
    let data;
    //console.log(query)
    if (query.id) {
      const json = await axios.get(process.env.api+'/newsDetails/id=' + query.id)
      data = json.data
      if (data && data.message) {
        data = { results: [] }
      }
      else {
        //console.log(data)
        data = { results: data.result[0] }
      }
      //const json = await res.json()
      //console.log(json)
      //return true //{ stars: 'json.stargazers_count' }
    }
    else {
      data = { results: [] }
    }
    return {
      props: data
    }
  } catch (e) {
    return {
      props: { results: [] }
    }
  }
}
export default NewsDetails;