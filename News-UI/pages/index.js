import styles from '../styles/Home.module.css'
import { useRouter } from 'next/router'

// import { useAmp } from 'next/amp'
// export const config = { amp: 'hybrid'  }
import Link from 'next/link'

 
function Home() {
     
    return (
        <div className={styles.container}>
             
            <p className={styles.description}>
                <a href="News?amp=1" className={styles.a}>
                    <button >AMP Page </button>
                </a> 
                <br/>
                <a href="News" className={styles.a}>
                    <button >Non-AMP Page </button>
                </a>
                 
            </p>
        </div >
    )
}
export default Home;