import Head from 'next/head'
import styles from '../styles/Home.module.css'

import { useAmp } from 'next/amp'
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Link from 'next/link'
import axios from "axios";

export const config = { amp: 'hybrid' }
const useStyles = makeStyles({
  table: {
    minWidth: 800,
  },
});


function News({ result }) {
  const isAmp = useAmp()
  const classes = useStyles();
  //const date = new Date()
 // console.log(isAmp)
  return (


    <div className={styles.container}>
      <Head>
        <title>News</title>
        <link rel="icon" href="/favicon.ico" /> <script
          async
          key="amp-timeago"
          custom-element="amp-timeago"
          src="https://cdn.ampproject.org/v0/amp-timeago-0.1.js"
        />
        <meta name="viewport" content="width=device-width,minimum-scale=1,initial-scale=1" />

      </Head>

      <main className={styles.main} style={{ textAlign: "center" }}>
        <h1 className={styles.title}>
          Welcome to <a href="javascript:void(0)">News</a>
        </h1>

        <div className={styles.grid}>
          <TableContainer component={Paper}>
            <Table className={classes.table} aria-label="simple table">
              <TableHead>
                <TableRow>
                  <TableCell> ID </TableCell>
                  <TableCell align="right">Category</TableCell>
                  <TableCell align="right">Headline</TableCell>
                  <TableCell align="right">Summary &nbsp;(g)</TableCell>
                  <TableCell align="right">Tags&nbsp;(#)</TableCell>
                  <TableCell align="right">Date&nbsp;(ISO)</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {result && result.length > 0 && result.map((row) => (
                  <TableRow key={row.id}>
                    <TableCell component="th" scope="row">
                    {isAmp ? <Link href={{ pathname: 'NewsDetails', query: { 'id': row.id , 'amp': 1} }}
                      //href={"/NewsDetails?id="+row.id}
                      ><a>{row.id}</a></Link> : 
                      <Link href={{ pathname: 'NewsDetails', query: { 'id': row.id  } }}
                      //href={"/NewsDetails?id="+row.id}
                ><a>{row.id}</a></Link> }
                    </TableCell>
                    <TableCell align="right">{row.category_name}</TableCell>
                    <TableCell align="right">{row.headline}</TableCell>
                    <TableCell align="right">{row.summary}</TableCell>
                    <TableCell align="right">{row.tags}</TableCell>
                    <TableCell align="right">
                      {isAmp ? <amp-timeago
                        width="0"
                        height="15"
                        datetime={row.created_at}
                        layout="responsive"
                      > . </amp-timeago> : row.created_at}</TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </TableContainer>

        </div>


      </main>

      <footer className={styles.footer} style={{ textAlign: "center" }}>

        Powered by {'News'}

      </footer>
    </div>

  )
}
export async function getServerSideProps(ctx) {
  try {
    let { data } = await axios.get(process.env.api+'/news')
    if (data && data.message) {
      data = { results: [] }
    }
    //const json = await res.json()
    //console.log(data)
    //return true //{ stars: 'json.stargazers_count' }
    return {
      props: data
    }
  } catch (e) {
    return {
      props: { results: [] }
    }
  }
}
export default News;