import {React, useContext} from 'react'
import News from "./News";
import { useRouter } from "next/router"; 
import '../styles/globals.css'

function MyApp({ Component, pageProps }) {
  let allowed = false;
  const router = useRouter();
    if (router.pathname.startsWith("/News") ) {
      allowed = !false;
    }
    if (router.pathname.startsWith("/NewsDetails") ) {
      allowed = !false;
    }
  const Components = allowed ? Component : News; 
  return <Component {...pageProps} />
}

export default MyApp
