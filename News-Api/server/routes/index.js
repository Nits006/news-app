const express = require('express');
const router = express.Router();

const model = require('../model/index.js');

router.get('/news',async (req, res, next) => {
   try {
    const getNews = await model.getNews();
    res.json({ result : getNews});
   } catch(error) {
    res.send({'message': 'error '+error})
   }
})

router.get('/newsDetails/:id',async (req, res, next) => {
   try {
      const id = req.params.id;
      const getNewsDetails = await model.getNewsDetails(id);
    res.json({ result : getNewsDetails});
   } catch(error) {
    res.send({'message': 'error '+error})
   }
})
module.exports =router;