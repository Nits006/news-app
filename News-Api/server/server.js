const express = require('express');
const apiRouter = require('./routes/index.js')
const app = express();
app.use(express.json());
app.use('/api', apiRouter);
app.use(function (req, res, next) {
  console.log('Time:', Date.now())
  next()
})
const dotenv = require('dotenv');
dotenv.config({ path: __dirname + `/../.env`})
 
app.listen(process.env.PORT || 5000 , () => {
  console.log(`Server is running on port: ${process.env.PORT || '5000'}`)
});

app.get('/', (req, res) => {
  res.send('Api!')
})

