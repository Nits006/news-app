var mysqlConf = require('../db/index.js').mysql_pool;


const getNews = async function getNews() {
  return new Promise(function (resolve, reject) {
    mysqlConf.getConnection(function (err, connection) {
      if (err) {
        reject(err);
      } else {
        connection.query('Select news_details.*, categories.category_name from news_details INNER JOIN categories ON news_details.id = categories.id; ', function (err, result) {
          connection.release();   //---> don't forget the connection release.
          console.log('Data received from Db:');
          if (result) {
            resolve(result);
          } else {
            reject(err);
          }
        });
      }
    })
  });
}

const getNewsDetails = async function getNewsDetails(id) {
  if(!id) {
    return Error('id is null');
  }
  return new Promise(function (resolve, reject) {
    mysqlConf.getConnection(function (err, connection) {
      if (err) {
        reject(err);
      } else {
        connection.query('Select news_details.*, categories.category_name from news_details INNER JOIN categories ON news_details.id = categories.id where news_details.'+id, function (err, result) {
          connection.release();   //---> don't forget the connection release.
          console.log('Data received from Dd -');
          if (result) {
            resolve(result);
          } else {
            reject(err);
          }
        });
      }
    })
  });
}

module.exports.getNews = getNews;
module.exports.getNewsDetails = getNewsDetails; 
