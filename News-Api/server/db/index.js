const mysql = require('mysql');
require('dotenv').config({ path: __dirname + `/../../.env`})

//console.log(`Server is running on CONNECTION_URLDB: ${process.env.CONNECTION_URLDB || '5000'}`)
const config = {
  mysql_pool: mysql.createPool({
    host: process.env.CONNECTION_URLDB || 'db4free.net',
    user: process.env.USERNAME_DB || 'cb_mysql',
    password: process.env.PASSWORD_DB || 'rootroot',
    database: process.env.DATABASENAME || 'cb_newsdb1',
   // port: process.env.DB_PORT || '3306',
    connectionLimit: 10
  })
};
module.exports = config;
